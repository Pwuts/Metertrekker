EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Module:WeMos_D1_mini U1
U 1 1 5DAB6A8E
P 4000 3550
F 0 "U1" H 4000 2661 50  0000 C CNN
F 1 "WeMos_D1_mini" H 4000 2570 50  0000 C CNN
F 2 "Module:WEMOS_D1_mini_light" H 4000 2400 50  0001 C CNN
F 3 "https://wiki.wemos.cc/products:d1:d1_mini#documentation" H 2150 2400 50  0001 C CNN
	1    4000 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 4350 4000 4500
Wire Wire Line
	4000 4500 4750 4500
Wire Wire Line
	6000 4500 6000 3750
Wire Wire Line
	6000 3750 6450 3750
Wire Wire Line
	6450 3250 6000 3250
Wire Wire Line
	6000 2500 5350 2500
Wire Wire Line
	3900 2500 3900 2750
Connection ~ 3900 2500
Connection ~ 4000 4500
Wire Wire Line
	5900 3350 6450 3350
Connection ~ 5350 2500
Wire Wire Line
	5350 2500 4750 2500
Wire Wire Line
	6450 3450 5600 3450
Wire Wire Line
	5600 3450 5600 4500
Connection ~ 5600 4500
Wire Wire Line
	5600 4500 6000 4500
Wire Wire Line
	6450 3650 5150 3650
Wire Wire Line
	5150 3650 5150 4100
$Comp
L Device:R R3
U 1 1 5DAC3927
P 4750 4300
F 0 "R3" H 4820 4346 50  0000 L CNN
F 1 "4k8" H 4820 4255 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4680 4300 50  0001 C CNN
F 3 "~" H 4750 4300 50  0001 C CNN
	1    4750 4300
	1    0    0    -1  
$EndComp
Connection ~ 4750 4100
Wire Wire Line
	4750 4100 5150 4100
Wire Wire Line
	4750 4150 4750 4100
Wire Wire Line
	4750 4450 4750 4500
Connection ~ 4750 4500
Connection ~ 4750 2500
Wire Wire Line
	4750 2500 3900 2500
Wire Wire Line
	4750 2500 4750 3750
Wire Wire Line
	4750 4100 4750 4050
$Comp
L Device:CP C1
U 1 1 5DACAF80
P 2850 3500
F 0 "C1" H 2968 3546 50  0000 L CNN
F 1 "470u" H 2968 3455 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D12.5mm_P5.00mm" H 2888 3350 50  0001 C CNN
F 3 "~" H 2850 3500 50  0001 C CNN
	1    2850 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 3350 2850 2500
Wire Wire Line
	2850 3650 2850 4500
$Comp
L Device:R R2
U 1 1 5DAC2DDC
P 4750 3900
F 0 "R2" H 4820 3946 50  0000 L CNN
F 1 "2k4" H 4820 3855 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4680 3900 50  0001 C CNN
F 3 "~" H 4750 3900 50  0001 C CNN
	1    4750 3900
	-1   0    0    1   
$EndComp
Text Label 4550 4100 0    50   ~ 0
Rx
Text Label 3450 3450 0    50   ~ 0
Rx
Wire Wire Line
	4550 4100 4750 4100
Wire Wire Line
	3450 3450 3600 3450
Wire Wire Line
	2850 2500 3900 2500
$Comp
L Transistor_FET:2N7000 Q1
U 1 1 5DABE3D3
P 5250 3150
F 0 "Q1" H 5456 3196 50  0000 L CNN
F 1 "2N7000" H 5456 3105 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline_Wide" H 5450 3075 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N7000.pdf" H 5250 3150 50  0001 L CNN
	1    5250 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 2950 5900 2950
$Comp
L Device:R R1
U 1 1 5DAC150B
P 5350 2750
F 0 "R1" H 5420 2796 50  0000 L CNN
F 1 "470" H 5420 2705 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5280 2750 50  0001 C CNN
F 3 "~" H 5350 2750 50  0001 C CNN
	1    5350 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 2600 5350 2500
Wire Wire Line
	4750 4500 5350 4500
Connection ~ 5350 4500
Wire Wire Line
	5350 4500 5600 4500
Wire Wire Line
	5900 2950 5900 3350
Wire Wire Line
	5350 2900 5350 2950
Connection ~ 5350 2950
Wire Wire Line
	5350 3350 5350 4500
Wire Wire Line
	5050 3150 5050 3450
$Comp
L Connector:RJ12 J1
U 1 1 5DAB7359
P 6850 3450
F 0 "J1" H 6520 3454 50  0000 R CNN
F 1 "RJ12" H 6520 3545 50  0000 R CNN
F 2 "Connector_RJ:RJ12_Amphenol_54601" V 6850 3475 50  0001 C CNN
F 3 "~" V 6850 3475 50  0001 C CNN
	1    6850 3450
	-1   0    0    1   
$EndComp
Wire Wire Line
	6000 2500 6000 3250
Wire Wire Line
	4400 3450 5050 3450
Wire Wire Line
	4050 2250 3300 2250
Wire Wire Line
	3300 2250 3300 4500
Connection ~ 3300 4500
Wire Wire Line
	3300 4500 4000 4500
Wire Wire Line
	4050 1600 4050 1750
Wire Wire Line
	2850 4500 3300 4500
Text Label 4250 1750 0    50   ~ 0
SH_IO1
Text Label 4250 2250 0    50   ~ 0
SH_IO2
Text Label 4400 3250 0    50   ~ 0
SH_SCL
Text Label 4400 3350 0    50   ~ 0
SH_SDA
Text Label 4150 2350 0    50   ~ 0
SH_SCL
Text Label 4150 1650 0    50   ~ 0
SH_SDA
Wire Wire Line
	4150 1650 4150 1750
Text Label 4400 3650 0    50   ~ 0
SH_IO1
Text Label 4400 3750 0    50   ~ 0
SH_IO2
Text Label 6000 2500 0    50   ~ 0
5V
Text Label 6000 4500 0    50   ~ 0
GND
Text Label 3750 1600 0    50   ~ 0
SH_3V3
$Comp
L Connector_Generic:Conn_02x03_Odd_Even SAO1
U 1 1 5DB1CAC6
P 4150 1950
F 0 "SAO1" V 4154 1762 50  0000 R CNN
F 1 "ShittyAddOn_v1.69bis" V 4245 1762 50  0000 R CNN
F 2 "Connector_PinSocket_2.54mm_mod:ShittyAddOn_v1.69bis" H 4150 1950 50  0001 C CNN
F 3 "~" H 4150 1950 50  0001 C CNN
	1    4150 1950
	0    -1   1    0   
$EndComp
Wire Wire Line
	4150 2250 4150 2350
Wire Wire Line
	4050 1600 3750 1600
Text Label 4100 2650 0    50   ~ 0
SH_3V3
Wire Wire Line
	4100 2650 4100 2750
$EndSCHEMATC

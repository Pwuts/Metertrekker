Metertrekker Hardware
---------------------

Circuit to request and read data from a DSMR compliant energy meter.
The circuit is powered with 5V from the P1 port (which must be able to supply
1.25W max according to the [spec]) so no separate power supply is needed.

[spec]: https://www.netbeheernederland.nl/_upload/Files/Slimme_meter_15_a727fce1f1.pdf

In October of 2019 I ordered a batch of 10 boards of the "d-shitty" version.
This hardware version + the [v1.0.0] [firmware](#firmware) version have run stably for months.

[v1.0.0]: https://github.com/Pwutseltronics/Metertrekker2MQTT/tree/v1.0.0

![Metertrekker v1.0d with Shitty Add-on connector](http://cdn.pwuts.nl/projects/metertrekker-layout-v1.0d-shitty.png)

The PCB can be used with a Wemos D1 mini (compatible) microcontroller board,
and since those have WiFi you can push the data to e.g. MQTT or Influx.

The "shitty" versions have a [Shitty Add-On v1.69bis](https://hackaday.io/project/52950-shitty-add-ons/log/159806-introducing-the-shitty-add-on-v169bis-standard) compliant Shitty Add-on connector! :D

Take care though that the 3V3 on the SAO connector comes from the LDO on the Wemos D1 mini,
so best don't short it and make sure your Wemos is not one of those cheap-ass Chinese copies with a 150mA max LDO.

BOM
---
* PCB
* Soldering stuff
* Wemos D1 mini
* 1:1 wired RJ12 cable

#### ICs
* 2N7000 (TO-92 package)

#### Resistors
* 470 Ω
* 2k4 Ω
* 4k7 Ω

#### Capacitors
* 220µF ≥6.3V (5mm lead spacing)

### Optional
* 2x03 2.54mm pin socket (for Shitty Add-on connector)

Variations
----------
I have made a couple of variations of this board:

* A: first version, lots of space, might add LEDs later
* B: different layout, also loosely spaced
* C (with or without SAO connector): more tightly spaced version of A
* D (with or without SAO connector): more tightly spaced version of B
* F: version of D with different (smaller) RJ12-connector

Firmware
--------
[![GitHub release (latest by date)](https://img.shields.io/github/v/release/Pwutseltronics/Metertrekker2MQTT?logo=github&label=Latest+release&style=for-the-badge)][latest release]

I wrote firmware to retrieve, decode and process telegrams from the meter:
[Pwutseltronics/Metertrekker2MQTT on GitHub]. It is configurable via WiFi,
uses SoftwareSerial for RX inversion, verifies the CRC16 of incoming telegrams
and can publish the data to MQTT (including in Influx format).

[latest release]: https://github.com/Pwutseltronics/Metertrekker2MQTT/releases
[Pwutseltronics/Metertrekker2MQTT on GitHub]: https://github.com/Pwutseltronics/Metertrekker2MQTT
